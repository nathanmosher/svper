'use strict';

// var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
// var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
require('colors');

var fs = require('graceful-fs');
var gulp = require('gulp');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var browserify = require('browserify');

var nodemon = require('gulp-nodemon');
var livereload = require('gulp-livereload');

var localConfig;

try {
  localConfig = require('./server/config/local.env');
} catch(e) {
  localConfig = {};
}

/**
 * Sass Compiling.
 * @type {Object}
 */
var plumberOpts = {
  errorHandler: function (err) {
    console.log('Plumber Error:', Object.keys(err));
  }
};

var sassOpts = {
  // errLogToConsole: true,
  style: 'compact',
  sourceMap: true,
  sourceMapEmbed: true,
  sourceMapContents: true,
  omitSourceMapUrl: false,
  includePaths: [
    'client/bower_components',
    'client/app',
    'client/components'
  ],
  onError: function (error) {
    console.log(
      '%s %s \nfile: %s\nline: %s\ncol:  %s',
      'Sass:'.red.bold,
      error.message.red,
      error.file.cyan,
      error.line.toString().cyan,
      error.column.toString().cyan);
  },
  onSuccess: function(result) {
    // result is an object: v2 change
    console.log('%s %s\nfile: %s\nms:   %s',
      'Sass:'.green.bold,
      'successfully compiled'.green,
      result.stats.entry.cyan,
      result.stats.duration.toString().cyan)
  }
};

var prefixerOpts = {
  browsers: ['last 3 versions'],
  cascade: false
};


gulp.task('sass', function() {

  gulp.src('./client/app/app.scss')
    // .pipe(plumber(plumberOpts))
    .pipe(sourcemaps.init())
      .pipe(sass(sassOpts))
      .pipe(autoprefixer(prefixerOpts))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./.tmp/app/'))
    .pipe(livereload());

});









/**
 * Browserify Compiling
 */
var bundler = watchify(browserify('./client/experimental/index.js', watchify.args));
// add any other browserify options or transforms here
bundler.transform('brfs');



function bundle() {
  return bundler.bundle()
    // log errors if they happen
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    // optional, remove if you dont want sourcemaps
      .pipe(buffer())
      .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
      .pipe(sourcemaps.write('./')) // writes .map file
    //
    .pipe(gulp.dest('./.tmp'))
    .pipe(livereload());
};

gulp.task('js', bundle);

bundler.on('log', function (msg) {
  console.log('watchify:', msg)
});

// bundler.on('time', function (time) {
//   // console.log('watchify:', time)
// });





/**
 * Restart Server on change.
 */
gulp.task('develop', function () {
  nodemon({
      script: 'server/app.js',
      ext: 'html js',
      ignore: ['node_modules'],
      env: { 'NODE_ENV': 'development' }
    })
    .on('change')
    .on('restart', function () {
      console.log('restarted!')
      // livereload.reload();
    })
});



gulp.task('livereload', function() {
  livereload.listen();
});








/**
 * Watching
 */
gulp.task('watch', ['js', 'sass', 'livereload', 'develop'], function() {
  gulp.watch('./client/**/*.scss', ['sass']);
  // on any dep update, runs the bundler
  bundler.on('update', bundle);
  // gulp.watch(['./views/**/*.html', './js/**/*.js'], ['js']);
  //
  livereload.listen();
  // gulp.watch('client/*.html', livereload);
  // gulp.watch('client/*.css', livereload);
});








/**
 * Build
 */
gulp.task('build', ['js', 'sass'], function() {
  gulp.watch('./client/**/*.scss', ['sass']);
  // on any dep update, runs the bundler
  bundler.on('update', bundle);

  // gulp.watch(['./views/**/*.html', './js/**/*.js'], ['js']);
});







gulp.task('default', ['sass', 'js', 'watch']);
