/**
 * Main application routes
 */

'use strict';
var path = require('path');
var errors = require('./components/errors');
// var auth = require('./auth/auth.service')

module.exports = function(app, io) {

  // Insert routes below
  app.use('/api/rdio', require('./api/rdio'));

  // app.use('/api/rooms/latest', require('./api/message/message.controller').latestRooms);

  app.use('/api/rooms', require('./api/room'));

  app.use('/api/messages', require('./api/message'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {

      var relPath = path.normalize(app.get('appPath') + '/index.html')
      var absPath = path.resolve(relPath);
      res.sendFile(absPath);
    });
};
