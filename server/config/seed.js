/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Message = require('../api/message/message.model');
var User = require('../api/user/user.model');

// Message.find({}).remove(function() {
//   // Message.create({
//   //   room : 'Development Tools',
//   //   user:
//   //   texts : [{
//   //     text: 'hey breh',
//   //     created: new Date();
//   //   }]
//   // });
//   //
//   // console.log('removed all messages')
// });

// User.find({}).remove(function() {
//   User.create({
//     provider: 'local',
//     name: 'Test User',
//     email: 'test@test.com',
//     password: 'test'
//   }, {
//     provider: 'local',
//     role: 'admin',
//     name: 'Admin',
//     email: 'admin@admin.com',
//     password: 'admin'
//   }, function() {
//       console.log('finished populating users');
//     }
//   );
// });
