/**
 * Socket.io configuration
 */

'use strict';

var config = require('./environment');
var User = require('../api/user/user.model')

// When the user disconnects.. perform this
function onDisconnect(socket) {
}

// When the user connects.. perform this
function onConnect(socket) {
  // When the client emits 'info', this listens and executes

  socket.on('info', function (data) {
    console.info('[%s] %s', socket.address, JSON.stringify(data, null, 2));
  });

  User.findById(socket.decoded_token._id, function (err, user) {
    if(err) return console.log('no user found');

    socket.user = user.toObject();
    socket.user.socket = {
      id: socket.id
    };
    // Insert sockets below
    require('../api/room/room.socket').register(socket);
    require('../api/message/message.socket').register(socket);
  });


}

module.exports = function (io) {
  // socket.io (v1.x.x) is powered by debug.
  // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
  //
  // ex: DEBUG: "http*,socket.io:socket"

  // We can authenticate socket.io users and access their token through socket.decoded_token
  //
  // 1. You will need to send the token in `client/components/socket/socket.service.js`
  //
  // 2. Require authentication here:
  io.use(require('socketio-jwt').authorize({
    secret: config.secrets.session,
    handshake: true
  }));

  io.on('connection', function (socket) {

    socket.address = socket.handshake.address !== null ?
            socket.handshake.address.address + ':' + socket.handshake.address.port :
            process.env.DOMAIN;

    socket.connectedAt = new Date();

    socket.on('error', function (err) { console.log('SOCKET ERROR', err)});
    socket.on('reconnect', function () { console.log('SOCKET RECONNECT')});
    socket.on('reconnect_attempt', function () { console.log('SOCKET RECONNECT ATTEMPT')});
    socket.on('reconnect_error', function () { console.log('SOCKET RECONNECT ERROR')});
    socket.on('reconnect_failed', function () { console.log('SOCKET RECONNECT FAILED')});
    // Call onDisconnect.
    socket.on('disconnect', function () {
      // Tell all clients when we leave a room, let them figure out whether to alert for now.
      io.sockets.emit('room:roomie:left', socket.user);
      console.info('[%s] DISCONNECTED', socket.address);
    });

    // Call onConnect.
    onConnect(socket);

    console.info('[%s] CONNECTED', socket.address);

    // On room join.
    socket.on('room:join:request', function(room) {

      var roomId = room._id.toString();

      console.log('join request', roomId)
      // Log for fun

      // Join that room.
      socket.join(roomId, function (err, success) {
        if(!err) {
          console.info('%s joined room %s', socket.user.name, roomId);
        } else {
          console.info('ERROR: %s joining room %s failed', socket.user.name, roomId);
        }

      });

      // Tell new roomie they made it, and give them current roomies.
      socket.emit('room:join:success', getUsersInRoom(roomId, io));

      // Tell everyone in the room.
      socket.to(roomId).emit('room:roomie:joined', socket.user);

    });

  });
};


function getUsersInRoom(room, io) {
  var namespace = '/';
  var users = [];
  // console.log('GET USERS IN ROOM', io['nsps'])
  //Find all users connected to the room.
  for (var socketId in io.nsps[namespace].adapter.rooms[room]) {
    // console.log(socketId)
    var user = io.sockets.connected[socketId].user;
    users.push(user);
  }
  return users;
};


