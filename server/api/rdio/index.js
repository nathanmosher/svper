'use strict';

var express = require('express');
var controller = require('./rdio.controller');

var router = express.Router();

router.get('/:method', controller.index);

module.exports = router;
