'use strict';

var _ = require('lodash');
var changeCase = require('change-case');

var Rdio = require('rdio')({
  rdio: {
    clientId: process.env.RDIO_KEY,
    clientSecret: process.env.RDIO_SECRET
  }
});

var rdio = new Rdio();

// pass request to rdio..
exports.index = function(req, res) {

  var method = changeCase.camelCase(req.params.method);
  var query = req.query;

  // hack for invalid search chars.
  if(query.query != undefined) {
    query.query = query.query.replace(/[^A-Za-z0-9$ ]/g, '');
  }
  console.log(rdio)

  // rdio.call(method, query, function (err, data) {
  //   if(err) handleError(res, err);
  //   console.log('data', query, data)
  //   if(data == undefined) {
  //     return res.send(500, {result:'i have no fuckin idea what happened here.'});
  //   }
  //   res.json(200, data)
  // });

  rdio.getClientToken(function(err) {
    if(err) handleError(res, err);
    var opts = _.merge({}, {method: method}, query);
    rdio.request(opts, false, function(err, data) {
      if(data == undefined) {
        return res.send(500, {result:'i have no fuckin idea what happened here.'});
      }
      res.json(200, data)
    });
  });

};

function handleError(res, err) {
  return res.send(500, err);
}
