'use strict';

var express = require('express');
var controller = require('./room.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/latest', controller.latest);
router.get('/:id', auth.authenticate(), controller.show);
router.post('/', auth.authenticate(), controller.create);
router.put('/:id', auth.authenticate(), controller.update);
router.patch('/:id', auth.authenticate(), controller.update);
router.delete('/:id', auth.authenticate(), controller.destroy);

module.exports = router;
