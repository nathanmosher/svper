'use strict';

var _ = require('lodash');
var Room = require('./room.model');

// Get list of rooms
exports.index = function(req, res) {
  var query = req.params;
  query = _.merge(query, req.query);

  // console.log('room show query', query)

  Room.find(query, function (err, rooms) {
    if(err) { return handleError(res, err); }
    if(!rooms.length) { return res.send(404); }
    return res.json(
      _.map(rooms, function (room) {
        return room.toObject({virtuals: true});
      })
    )
  });
};

// Get list of rooms
exports.latest = function(req, res) {
  var query = req.params;
  query = _.merge(query, req.query);

  // console.log('room latest query', query)

  Room.find(query)
    .sort('-modified')
    .limit(100)
    .exec(function (err, rooms) {
    if(err) { return handleError(res, err); }
    if(!rooms.length) { return res.send(404); }
    return res.json(
      _.map(rooms, function (room) {
        return room.toObject({virtuals: true});
      })
    );
  });
};

// Get a single room
exports.show = function(req, res) {
  var query = req.params;
  // console.log('room show query', query)
  Room.findOne(query, function (err, room) {
    if(err) { return handleError(res, err); }
    if(!room) { return res.send(404); }
    return res.json(room.toObject({virtuals: true}));
  });
};

// Get a single room by name
exports.getByName = function(req, res) {

  Room.find({name:req.params.name}, function (err, room) {
    if(err) { return handleError(res, err); }
    if(!room) { return res.send(404); }
    return res.json(room.toObject({virtuals: true}));
  });
};

// Get a single room by name
exports.getBySlug = function(req, res) {

  Room.find({name:req.params.slug}, function (err, room) {
    if(err) { return handleError(res, err); }
    if(!room) { return res.send(404); }
    return res.json(room.toObject({virtuals: true}));
  });

};

// Creates a new room in the DB.
exports.create = function(req, res) {
  Room.create(req.body, function(err, room) {
    if(err) { return handleError(res, err); }
    return res.json(201, room.toObject({virtuals: true}));
  });
};

// Updates an existing room in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }


  Room.findById(req.params.id, function (err, room) {
    if (err) { return handleError(res, err); }
    if(!room) { return res.send(404); }

    var updated = _.merge(room, req.body);

    if(req.body.queue) {
      updated.queue = req.body.queue;
    }

    updated.save(function (err, doc) {
      if (err) { return handleError(res, err); }

      var newRoom = doc.toObject({virtuals: true});

      var io = req.app.get('io');

      // tell everyone in the room about the update.
      io.to(req.params.id).emit('room:save', newRoom);

      // return that shit to the client
      return res.json(200, newRoom);
    });
  });
};

// Deletes a room from the DB.
exports.destroy = function(req, res) {
  Room.findById(req.params.id, function (err, room) {
    if(err) { return handleError(res, err); }
    if(!room) { return res.send(404); }
    room.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  console.dir(err);
  return res.json(500, err);
}
