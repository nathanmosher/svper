'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var troop = require('mongoose-troop');
var _ = require('lodash');
var moment = require('moment');
var s = require('underscore.string');

var RoomSchema = new Schema({
  name: { type: String, required: true },
  slug: { type: String },
  info: String,
  active: Boolean,
  queue: [{
    key: { type: String },
    duration: { type: Number },
    title: { type: String }
  }],
  track: {
    started: { type: Date, default: null },
    index: { type: Number, default: -1 },
    duration: { type: Number, default: -1 },
    key: { type: String, default: null },
    title: { type: String, default: null }
  }
});

RoomSchema
  .pre('validate', function(next) {

    var self = this;
    self.slug = s.slugify(self.name);
    next();

});

  // Validate room slug is not taken
RoomSchema
  .path('slug')
  .validate(function(value, respond) {
    var self = this;

    self.slug = s.slugify(self.name);

    self.constructor.findOne({name: value}, function(err, room) {
      if(err) throw err;
      if(room) {
        if(self.id === room.id) return respond(true);
        return respond(false)
      }
      return respond(true);
    });
}, 'The url /r/{VALUE} is already taken :/');


RoomSchema.plugin(troop.timestamp, {useVirtual: false});

RoomSchema.post('init', function() {
  this._original = this.toObject();
});

// Validate room name is not taken
RoomSchema
  .path('name')
  .validate(function(value, respond) {
    var self = this;
    self.constructor.findOne({name: value}, function(err, room) {
      if(err) throw err;
      if(room) {
        if(self.id === room.id) return respond(true);
        return respond(false)
      }
      return respond(true);
    });
}, 'The specified room name {VALUE} is already taken.');



// Validate queue changes are to unplayed tracks.
RoomSchema
  .path('queue')
  .validate(function(doc) {

    var self = this;

    var queue = doc.toObject();

    if(self._original == undefined) {
      return true;
    }

    var older = self._original.queue.slice(0, self._original.track.index*1+1);

    var newer = queue.slice(0, self._original.track.index*1+1);

    var valid = _.isEqual(older, newer);

    return valid;

}, 'Editing queue history is not allowed.');

// Validate the current track change is valid.
RoomSchema
  // .path('track')
  // .validate(function(track) {
  .pre('save', function(next) {

    var self = this;

    self.track;

    var newIndex = self.track.toObject().index;

    // if there is no original track.
    if (self._original == undefined) {
      self._original = {
        track: {
          index: -1,
          duration: -1,
          started: null,
          key: null,
          title: null
        }
      }
    }

    var oldTrack = self._original.track;

    var oldIndex = oldTrack.index;

    var queue = self.queue;

    var trackIsOver = isTrackOver(self.track);

    var canAdvance = !!queue[oldIndex+1];

    var needsToAdvance = (trackIsOver && canAdvance);

    if (needsToAdvance) {

      self.track = {
        index: oldIndex+1,
        duration: queue[oldIndex+1].duration,
        started: new Date(),
        key: queue[oldIndex+1].key,
        title: queue[oldIndex+1].title
      }
      newIndex = self.track.index;
    }

    // invalid if the new index is outside the queue length.
    if (newIndex > queue.length) {
      self.invalidate('track', newIndex + ' is an invalid queue index.');
      return next(new Error(newIndex + ' is an invalid queue index.'));
    }

    // invalid if the queue is less than -1.
    if (newIndex < -1) {
      self.invalidate('track', newIndex + ' is an invalid queue index.');
      return next(new Error(newIndex + ' is an invalid queue index.'));
    }



    // if we aren't advancing the queue...
    if (newIndex == oldIndex) {
      var validTrack = _.isEqual(self.track.toObject(), oldTrack);
      if(validTrack) {

        return next();
      }else{
        self.invalidate('track', 'Cannot change currently playing track.');
        return next(new Error('Cannot change currently playing track.'));
      }
    }

    // but if we are advancing the queue.
    if (newIndex == oldIndex+1) {

      // check to see if we have anything in the queue
      if(!!queue[newIndex] && !!queue[newIndex].key) {

        // and if it is the first song, just play it
        if(isTrackOver(oldTrack)) {
          // track = _.merge(track, queue[newIndex], {started: new Date()})
          self.track = {
            index: oldIndex+1,
            duration: queue[oldIndex+1].duration,
            started: new Date(),
            key: queue[oldIndex+1].key,
            title: queue[oldIndex+1].title
          }
          console.log('kicking off next track:', self.track)
          return next();
        } else {
          self.invalidate('track', 'Current track is not over.');
          return next(new Error('Current track is not over.'));
        }

      } else {
        return next(new Error('No unplayed tracks, try adding some to queue.'));
      }

    }

    return next(new Error('Invalid queue position.'));

// }, 'Index must be in valid position in room queue.');
});


function isTrackOver (track) {
  if(track.started == null) {
    return true;
  }
  // get a moment object from when the last track started.
  var oldStartedMoment = moment(track.started);

  // get a moment object for right now.
  var nowMoment = moment();

  // get the difference bw now and song started in seconds.
  var diff = nowMoment.diff(oldStartedMoment, 'seconds');

  return diff > track.duration;
};

module.exports = mongoose.model('Room', RoomSchema);
