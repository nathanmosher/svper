/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Room = require('./room.model');

exports.register = function(socket) {
  // console.log(Room.schema.paths.queue.schema)
  // Room.schema.paths.queue.schema.post('save', function (doc) {
  //   console.log('saved queue', doc);
  // })

  // Room.schema.post('save', function () {
  //   console.log('room save')
  //   onSave(socket, arguments[0]);
  // });

  Room.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });

  socket.on('room:track-ended', function (userRoom) {
    onTrackOver(socket, userRoom);
  });

}

function onTrackOver (socket, userRoom, cb) {
  Room.findById(userRoom._id, function (err, room) {
    if (err) { return console.log('Could not find room %s', room.name); }
    // var updated = _.merge(room, req.body);
    room.track.index++;

    room.save(function (err, newRoom) {
      if(err) return console.log(err);
      console.log('track ended in room %s from user %s', newRoom._id, socket.user.name);
      var id = newRoom._id.toString();
      socket.to(id).emit('room:next-track', room.track.toObject());
    });
  });
}

// function onSave(socket, doc, cb) {
//   var id = doc._id.toString();
//   console.log('save handler called on room %s', id)
//   socket.to(id).emit('room:save', doc);
// }

function onRemove(socket, doc, cb) {
  socket.emit('room:remove', doc);
}

