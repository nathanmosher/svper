'use strict';

var express = require('express');
var controller = require('./message.controller');
var auth = require('../../auth/auth.service');


var router = express.Router({ mergeParams: true });

// router.get('/', controller.latestRooms);
router.get('/', controller.index);
// router.get('/:id', controller.show);
router.post('/', auth.authenticate(), controller.create);
router.put('/:id', auth.authenticate(), controller.update);
router.patch('/:id', auth.authenticate(), controller.update);
router.delete('/:id', auth.authenticate(), controller.destroy);

module.exports = router;
