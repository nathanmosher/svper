'use strict';

var mongoose = require('mongoose');
var troop = require('mongoose-troop');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var MessageSchema = new Schema({
  room: { type: ObjectId, ref: 'Room' },
  user: { type: ObjectId, ref: 'User' },
  text: String
});
MessageSchema.plugin(troop.timestamp, {useVirtual: false});
module.exports = mongoose.model('Message', MessageSchema);
