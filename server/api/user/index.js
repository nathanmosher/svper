'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router({ mergeParams: true });

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.authenticate(), controller.me);
router.put('/:id/password', auth.authenticate(), controller.changePassword);
router.get('/:id', auth.authenticate(), controller.show);
router.post('/', controller.create);

module.exports = router;
