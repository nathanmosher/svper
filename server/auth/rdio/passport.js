var passport = require('passport');
var RdioStrategy = require('passport-rdio-oauth2').Strategy;

exports.setup = function (User, config) {
  passport.use(
    new RdioStrategy({
      clientID: config.rdio.key,
      clientSecret: config.rdio.secret,
      callbackURL: config.rdio.callbackURL
    },
    function(token, tokenSecret, profile, done) {
      User.findOne({
        'rdio.key': profile.id
      }, function(err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          user = new User({
            name: profile.displayName,
            role: 'user',
            provider: 'rdio',
            rdio: profile._json.result
          });
          user.save(function(err) {
            if (err) return done(err);
            return done(err, user);
          });
        } else {
          console.log('user is already hangin!')
          return done(err, user);
        }
      });
    }
  ));
};
