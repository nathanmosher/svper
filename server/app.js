/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// if(process.env.NODE_ENV === 'production') {
//   require('newrelic');
// }

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);

// Populate DB with sample data
if(config.seedDB) { require('./config/seed'); }

// Setup server
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server, {
  serveClient: (config.env === 'production') ? false : true,
  path: '/socket.io-client'
});

// If we have redis config, use the redis adapter for socket.io.
if(config.redis && config.redis.uri) {
  var url = require('url');
  var redis = require('redis');
  var redisAdapter = require('socket.io-redis');

  var redisURL = url.parse(config.redis.uri);

  var pub = redis.createClient(redisURL.port, redisURL.hostname, {auth_pass:redisURL.auth.split(":")[1]});

  var sub = redis.createClient(redisURL.port, redisURL.hostname, {detect_buffers: true, auth_pass:redisURL.auth.split(":")[1]} );

  io.adapter( redisAdapter({pubClient: pub, subClient: sub}) );
}

require('./config/socketio')(io);
require('./config/express')(app);
require('./routes')(app);

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

app.set('io', io);

// Expose app
exports = module.exports = app;
