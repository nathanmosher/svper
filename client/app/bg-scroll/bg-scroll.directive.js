'use strict';

angular.module('svperApp')
  .directive('bgScroll', function ($window, $document) {
    return {
      restrict: 'EAC',
      link: function (scope, element, attrs) {
        var oldTop = 0;
        element.css('transform', 'translate3d(0,0,0)')
        if (!element.hasClass('navbar') && !element.hasClass('create-field'))
          return;

        var bgUrl = element.css('background-image');

        // console.dir(bgUrl)

        var stripped = bgUrl.replace('url(', '').replace(')', '');
        var stripped = stripped.replace(/\"/g, '');
        // console.dir(stripped);

        var img = angular.element('<img />');

        img.attr('src', stripped)

        element.css({'overflow': 'hidden'})

        img.css({
          position: 'absolute',
          width: '100vw',
          top: 0,
          left: 0,
          'z-index': -1
        });

        var img2 = img.clone()

        img2.css({
          position: 'absolute',
          width: '100vw',
          top: '100vh',
          left: 0
        });

        element.append(img)
        element.append(img2)
        var getPos = function () {
          var height = img.height();


          var bounding = element[0].getBoundingClientRect();
          var currentElementViewPortTop = element[0].getBoundingClientRect().top;

          var relYOffset = bounding.top * -1;
          var relXOffset = bounding.left * -1;

          var currentTop = angular.element($window).scrollTop() * -0.3;

          var offset = currentTop + 300 + relYOffset;

          var yCalc = (parseFloat(currentTop) + relYOffset) % height;

          oldTop = currentTop;

          return {
            y: yCalc,
            x: relXOffset
          };
        }

        var render = function () {
          var pos = getPos();
          img.css({transform: 'translate3d('+pos.x+'px, '+ pos.y +'px, 0'});
          img2.css({transform: 'translate3d('+pos.x+'px, '+ pos.y +'px, 0'});
          window.requestAnimationFrame(render)
        };

        // var render = function () {
        //   var pos = getPos()
        //   element.css({backgroundPosition: pos.x+'px '+ pos.y +'px'});
        //   // window.requestAnimationFrame(render)
        // };

        window.requestAnimationFrame(render)

        // angular.element($window).on('scroll', function () {

        //   window.requestAnimationFrame(render);

        // });




      }
    };
  });
