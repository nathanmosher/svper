'use strict';

describe('Directive: bgScroll', function () {

  // load the directive's module
  beforeEach(module('svperApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bg-scroll></bg-scroll>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bgScroll directive');
  }));
});