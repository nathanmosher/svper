'use strict';

angular.module('svperApp')
  .directive('autoFocus', function () {
    return {
      restrict: 'A',
      scope: {
        autoFocus: '='
      },
      link: function (scope, element, attrs) {
        element.text('this is the autoFocus directive');
        if(scope.autoFocus === true) {
          element[0].focus();
        };
        scope.$watch('autoFocus', function (oldVal, newVal) {
          if(newVal === true) {
            element[0].focus();
          }
        });
      }
    };
  });
