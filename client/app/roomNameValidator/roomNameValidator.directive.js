'use strict';

angular.module('svperApp')
  .directive('roomNameValidator', function ($http, $q) {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attrs, ngModel) {
        ngModel.$asyncValidators.roomNameAvailability = function(modelValue, viewValue) {

          var d = $q.defer();
          // if(viewValue == '') {
          //   d.reject('room cannot be empty string');
          // }
          $http({
            method: 'GET',
            url: '/api/rooms/',
            params: {
              name: viewValue
            }
          }).then(function(res) {
            // return false;
            ngModel.$setViewValue(viewValue)
            return d.reject('room name is taken :/');
          }, function (res) {
            return d.resolve(true);
          });

          return d.promise;
        };
      }
    };
  });
