'use strict';

angular.module('svperApp')
  .service('rdioService', function ($q, $http, $cacheFactory, localStorageService) {

    var rdioCache = $cacheFactory('rdio-cache');

    // AngularJS will instantiate a singleton by calling "new" on this function
    // var rdio = function (params) {
      var self = this;

      self.apiUrl = '/api/rdio/';

      self.request = function (method, params) {
        return $http({
          method: 'GET',
          url: self.apiUrl + method,
          params: params
        });
      };

      self.search = function (params) {
        return self.request('search', params).then(function(res) {
          // _.each(res.data.results)

          if(res.data && res.data.result && res.data.result.results) {
            _.each(res.data.result.results, function (value, i) {
              localStorageService.set(value.key, value)
            });
          }
          return res;
        });
      };

      self.searchTracks = function (params) {
        params.types = 'track';
        return self.search(params);
      };

      self.searchSuggestions = function (params) {
        params.types = 'track, artist, album';
        return self.request('search-suggestions', params);
      };

      self.searchTrackSuggestions = function (params) {
        params.types = 'track';
        return self.request('search-suggestions', params);
      };

      self.getTracksForArtist = function (params) {
        return self.request('get-tracks-for-artist', params);
      };

      self.getPlaybackToken = function (params) {
        return self.request('get-playback-token', params);
      };

      self.get = function (list) {
        var list = _.pluck(list, 'key');

        var cached = {};

        var unCachedKeys = _.filter(list, function (value, i) {
          var item = localStorageService.get(value);
          // snatch the cached value.
          if(!!item) {
            cached[value] = item;
          }

          // returns true if we are *not* cached.
          return !item;
        });

        // if we already have everything, don't bother actually making a request.
        if(!unCachedKeys.length) {
          var d = $q.defer();
          d.resolve({ data: { result: cached } });
          return d.promise;
        }

        // build the query string to pass up.
        var str = unCachedKeys.reverse().join(',');

        //
        return self.request('get', {
          keys: str
        }).then(function (res) {
          _.each(res.data.result, function (value, key) {
            localStorageService.set(key, value)
          });

          // merge in our results with our cached.
          _.merge(res.data.result, cached);

          return res;
        });
      };

      return self;

  });
