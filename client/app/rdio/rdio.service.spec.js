'use strict';

describe('Service: rdio', function () {

  // load the service's module
  beforeEach(module('svperApp'));

  // instantiate service
  var rdio;
  beforeEach(inject(function (_rdio_) {
    rdio = _rdio_;
  }));

  it('should do something', function () {
    expect(!!rdio).toBe(true);
  });

});
