'use strict';

angular.module('svperApp')
  .controller('RoomCtrl', function ($rootScope, $scope, $stateParams, $http, $timeout, room, socket, playerService) {
    $scope.room = room;
    $scope.messages = $scope.room.messages;
    $scope.searching = false;

    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams){
        // console.log('state', fromParams)
        if (fromParams.room != toParams.room) {
          playerService.then(function (player) {
            player.pause();
          });
        }

      });

    $scope.usersList = function () {
      return _.uniq($scope.users, function(user) { return user._id; });
    };

    $scope.typing = false;
    $scope.typingId = null;
    $scope.typingTimeout = null;

    $scope.newMessage = {
      room: $scope.room._id
    };

    $scope.cutMessage = function () {
      clearTimeout($scope.typingTimeout);
      $scope.typing = false;

      // PUT new message
      $http({
        method: 'PUT',
        url: '/api/messages/'+$scope.typingId,
        data: $scope.newMessage
      }).success(function () {
        $scope.typingId = null;
        $scope.newMessage.text = '';
      });
    };

    // This runs on change to message.text.
    $scope.sendMessage = function () {
      // If we are typing
      if($scope.typing) {
        // And we have a message ID from original message POST
        if($scope.typingId) {

          // PUT new message/
          $http({
            method: 'PUT',
            url: '/api/messages/'+$scope.typingId,
            data: $scope.newMessage
          });
        }

        // Regardless if we have an ID or not, extend the timeout.
        clearTimeout($scope.typingTimeout);
        $scope.typingTimeout = setTimeout(function(){
          $scope.typing = false;
          $scope.typingId = null;
          $scope.newMessage.text = '';
          $scope.$apply();
        }, 1500);
      } else {

        $scope.typing = true;

        $scope.typingTimeout = setTimeout(function(){
          $scope.typing = false;
          $scope.typingId = null;
          $scope.newMessage.text = '';
          $scope.$apply();
        }, 1500);

        $http({
          method: 'POST',
          url: '/api/messages',
          data: $scope.newMessage
        }).success(function(data) {
          $scope.typingId = data._id;
        });
      }

    };

    socket.emit('room:join:request', $scope.room);
    $rootScope.currentRoom = $scope.room;

    socket.on('room:join:success', function (users) {
      $scope.users = users;
    });

    socket.on('room:roomie:joined', function (user) {
      console.log('new roomie %s in room %s', user.name, $scope.room);
      $scope.users.push(user);
    });

    // This event gets sent to all rooms, so we check to see if we care about it.
    socket.on('room:roomie:left', function (user) {
      var index = _.findIndex($scope.users, function (existingUser) {
        return existingUser.socket.id === user.socket.id;
      });
      if(index !== -1) {
        console.log('disconnected:', $scope.users[index]);
        $scope.users.splice(index, 1)
      }
    });

    socket.on($scope.room._id+':save', function(data) {
      var index = _.findIndex($scope.messages, {_id: data._id});
      console.log('new message')
      // If we already have the message, update it.
      if (index !== -1) {
        $scope.messages[index] = data;

      // Else push it on the array.
      } else {
        $scope.messages.push(data);
      }
    });

    // socket.on('event', function (data) {
    //   console.log('i made it!', data)
    // })

  });
