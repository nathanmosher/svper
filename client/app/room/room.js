'use strict';

angular.module('svperApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('room', {
        url: '/r/:room',
        templateUrl: 'app/room/room.html',
        controller: 'RoomCtrl',
        authenticate: true,
        resolve: {
          // This code queries for a room, if not found, creates a new one.
          room: function(Auth, $http, $stateParams, $location, $cookieStore) {

            var apiUrl = '/api/rooms/';
            var room;

            console.log($location.$$url)

            if($location.$$url) {
              $cookieStore.put('attempted', $location.$$url);
            }

            var p = $http({
              method: 'GET',
              url: apiUrl,
              params: {
                slug: $stateParams.room
              }
            });

            var m = p.then(function (res) {
              room = res.data[0] || res.data;

              var apiUrl = '/api/messages';
              return $http({
                method: 'GET',
                url: apiUrl,
                params: {
                  room: room._id
                }
              })
            }).then(function (res) {
              room.messages = res.data;
              return room;
            });

            return m;
          },
          socket: function(socketPromise) {
            return socketPromise;
          }
        }
      })
      .state('room.search', {
        url: '/search?q',
        templateUrl: 'components/search/search-results.html',
        controller: 'SearchResultsCtrl',
        authenticate: true
      });
  });
