'use strict';

angular.module('svperApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/?create',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });
  });
