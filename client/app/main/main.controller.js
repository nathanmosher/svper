'use strict';

angular.module('svperApp')
  .controller('MainCtrl', function (Auth, $cookieStore, $location, $scope, $http, socketPromise, $state) {

    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.roomName = $state.params.create;
    $scope.errors = {};
    $http.get('/api/rooms/latest').success(function(rooms) {
      $scope.rooms = rooms;
    });

    $scope.createRoom = function (createForm) {
      $http({
        method: 'POST',
        url: '/api/rooms/',
        data: {
          name: $scope.roomName
        }
      }).then(function (res) {
        $state.go('room.search', {
          room: res.data.slug
        });
      }, function (res) {

        if(res.status == 401) {
          $state.go('main');
        }
        if (res.status == 500) {
          if(res.data.errors) {
            $scope.errors.mongoose = res.data.errors;
          }
        }
      });
    };

  });
