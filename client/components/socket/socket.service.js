/* global io */
'use strict';

angular.module('svperApp')
  .service('socketPromise', function(socketFactory, Auth, $q, $window) {

    // derp the promise.
    var socket = $q.defer();

    // get our user so we can auth our sockets with a token
    Auth.getCurrentUser().$promise.then(function (user) {

        var domain = '127.0.0.1:9000';
        var path = '/socket.io-client';

        // cloudflare doesn't support sockets so hit heroku.
        if($window.location.host == 'beta.svper.io') {
          domain = 'beta-svper.herokuapp.com';
        }

        // create the socket
        var newSocket = (function() {
          return socketFactory({
            ioSocket: io(domain, {
              // Send auth token on connection, you will need to DI the Auth service above
              'query': 'token=' + user.token,
              'path': path,
              'reconnection': true,
              'reconnectionDelay': 1000,
              'reconnectionDelayMax' : 5000,
              'reconnectionAttempts': 5
            })
          });
        })();

        // resolve the promise
        socket.resolve(newSocket);
    });

    // return the promise
    return socket.promise;
  });
