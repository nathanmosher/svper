'use strict';

angular.module('svperApp')
  .service('playerService', function(socketPromise, $stateParams, $rootScope, $window, $q, rdioService) {

    var deferred = $q.defer();

    R.ready(function() {
      var user = R.currentUser.toJSON();
      console.log(user)
      R.player.on('change:isMaster', function (e, state) {
        console.log('change:isMaster', e)
      });
      R.player.on('change:playingSource', function (e, state) {
        console.log('change:playingSource', e)
      });
      R.player.on('change:playingTrack', function (e, state) {
        console.log('change:playingTrack', e)
      });
      R.player.on('change:playState', function (e, state) {
        console.log('change:playState', e)
      });
      R.player.on('change:position', function (e, state) {
        console.log('change:position', e)

      });
      R.player.on('change:repeat', function (e, state) {
        console.log('change:repeat', e)
      });
      R.player.on('change:shuffle', function (e, state) {
        console.log('change:shuffle', e)
      });
      R.player.on('change:sourcePosition', function (e, state) {
        console.log('change:sourcePosition', e)
      });
      R.player.on('change:volume', function (e, state) {
        console.log('change:volume', e)
      });
      deferred.resolve(R.player);
    });

    // // Go get a playback token from rdio.
    // var tokenPromise = rdioService.getPlaybackToken({
    //   domain: $window.location.hostname
    // });

    // // Create player dom element.
    // var player = $('<div />');

    // // Append that shit.
    // $('body').append(player);

    // // Set an id because whatever why not... I think I don't need this now?
    // player.attr('id', 'player');

    // // After we got a token..
    // tokenPromise.then(function (res) {
    //   var token = res.data.result;
    //   player.rdio(token);
    // }, function (err) {
    //   deferred.reject(err);
    // });

    // // When rdio player tells us it is ready to play some shit, resolve the promise.
    // player.on('ready.rdio', function (e, user) {
    //   console.log('rdio ready for user', user);

    //   deferred.resolve(player);

    // });

    // player.on('playingSourceChanged.rdio', function (e, state) {
    //   console.log('player.service: playingSourceChanged', state);
    // });


    // player.on('updateFrequencyData.rdio', function (e, state) {
    //   console.log('player.service: updateFrequencyData', state);
    // });


    // player.on('playingSomewhereElse.rdio', function (e, state) {
    //   console.log('player.service: playingSomewhereElse', state);
    // });

    // player.on('freeRemainingChanged.rdio', function (e, state) {
    //   console.log('player.service: freeRemainingChanged', state);
    // });

    // player.on('playStateChanged.rdio', function (e, state) {
    //   console.log('player.service: playStateChanged', state);
    // });


    // $window.player = player;

    return deferred.promise;

  });
