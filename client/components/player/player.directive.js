'use strict';

angular.module('svperApp')
  .directive('player', function (playerService, $timeout) {
    return {
      templateUrl: 'components/player/player.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        scope.ready = false;
        scope.muted = false;

        playerService.then(function (player) {

          scope.ready = true;
          scope.percentPosition = 0;

          player.on('change:playState', function (e, playState) {
            console.log('dir', e)
            var states = ['paused', 'playing', 'stopped', 'buffering', 'paused'];
            $timeout(function () {
              scope.state = states[e];
              console.log('rdio play state changed to:', states[playState])
            }, 0);
          });

          player.on('change:playingTrack', function (e, playingTrack) {
           console.log('dir', e)
            $timeout(function () {
              scope.track = e.toJSON();
            }, 0);
          });

          player.on('change:position', function (e, position) {
            $timeout(function () {
              scope.position = e;

              if (scope.track && scope.track.duration) {
                scope.percentPosition = position/scope.track.duration*100;
              }

            }, 0);
          });

          scope.$watch('muted', function (newVal, oldVal) {
            player.volume(newVal);
          });

        });


      }
    };
  });
