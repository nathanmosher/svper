'use strict';

angular.module('svperApp')
  .controller('QueueCtrl', function ($scope, rdioService, playerService, $http, socketPromise, $timeout, $q, $rootScope) {

    $scope.query = '';
    $scope.metaQueue = [];

    $scope.unplayed = [];
    $scope.played = [];
    $scope.metaTrack = {};

    $scope.addToQueue = function (track) {
      var newq = _.cloneDeep($scope.room.queue) || [];
      newq.push({
        title: track.name,
        key: track.key,
        duration: track.duration
      });

      $http({
        method: 'PUT',
        url: '/api/rooms/'+$scope.room._id,
        data: {
          queue: newq
        }
      }).then(function (res) {
        console.log('put response from room %s', res.data.name, res.data);
        $scope.room = res.data;
      });
    };

    $scope.removeFromQueue = function (track) {
      var newq = _.cloneDeep($scope.room.queue) || [];

      var removed = _.remove(newq, function(queuedTrack) {
        return track._id == queuedTrack._id;
      });

      $http({
        method: 'PUT',
        url: '/api/rooms/'+$scope.room._id,
        data: {
          queue: newq
        }
      }).then(function (res) {
        console.log('put response from room %s', res.data.name, res.data);
        $scope.room = res.data;
      });
    };

    $rootScope.$on('queue:add', function (e, track) {
      console.log('adding track to queue from $root event', track, e);
      $scope.addToQueue(track);
    });

    // $scope.unplayed computed prop
    $scope.$watchCollection('metaQueue + room.track.index', function () {
      console.log('queue change, recomputing unplayed queue')
      var currentIndex = $scope.room.track.index + 1;
      var unplayed = $scope.metaQueue.slice(currentIndex, $scope.room.queue.length);
      $scope.unplayed = unplayed;

    });

    // $scope.played computed prop
    $scope.$watchCollection('metaQueue', function () {
      console.log('queue change, recomputing played queue')
      var currentIndex = $scope.room.track.index + 1;
      var played = $scope.metaQueue.slice(currentIndex, $scope.room.queue.length);
      $scope.played = played;
    });

    var getTrackMetaDebounced = _.debounce(function() {
      rdioService.get($scope.room.queue).then(function (res){
        $scope.metaQueue = _.map($scope.room.queue, function (track) {
          return _.merge({}, track, res.data.result[track.key]);
        });
      });
    }, 300, {leading: true, trailing: false});

    // when the queue changes, make a request to rdio for track meta data.
    $scope.$watchCollection('room.queue', function () {
      getTrackMetaDebounced();

    });



    // Sockets ready
    socketPromise.then(function (socket) {

      socket.on('room:save', function (room) {
        console.log('socket update from room %s: ', $scope.room.name, room)
        $scope.room = room;
      });

    });

    // when both rdio and our socket connection is authenticated, set up events.
    $q.all([socketPromise, playerService]).then(function (res) {
      var socket = res[0];
      var player = res[1];

      player.on('positionChanged.rdio', function (e, position) {

        var track = $scope.room.track;

        if (isTrackOver(track)) {
          console.log('track ended', $scope.room)
          // socket.emit('room:track-ended', $scope.room);
          $http({
            method: 'PUT',
            url: '/api/rooms/'+$scope.room._id,
            data: {
              track: {
                index: track.index+1
              }
            }
          }).then(function (res) {
            console.log('response from PUT advancing track', res);
          });
        }

      });
    });

    var endTrack = function () {
      return $http({
        method: 'PUT',
        url: '/api/rooms/'+$scope.room._id,
        data: {
          track: {
            index: $scope.room.index+1
          }
        }
      }).then(function (res) {
        console.log('response from PUT advancing track', res);
        return res;
      });
    };

    var isTrackOver = function (track) {

      if(track.started == null) {
        return true;
      }
      var start = moment(track.started);
      var now = moment();

      var diff = now.diff(start, 'seconds');

      return diff > track.duration;
    };

    $scope.isCurrentTrackOver = function () {
      return isTrackOver($scope.room.track);
    };

    $scope.isQueueingNextTrack = function () {
      return $scope.isCurrentTrackOver() && $scope.unplayed.length > 0;
    };

    $scope.isQueueExhausted = function () {
      return $scope.isCurrentTrackOver() && $scope.unplayed.length == 0;
    };

    // to tell the player where to start.
    var getTrackPosition = function (started) {
      var start = moment(started);
      var now = moment();

      var diff = now.diff(start, 'seconds');

      return diff;
    }

    // When the current track is changed, handle it.
    $scope.$watch('room.track', function (oldRoom, newRoom) {

      var track = $scope.room.track;
      console.log(track)
      rdioService.get([track]).then(function (res) {
        $scope.metaTrack = res.data.result[track.key];
      });

      if(!$scope.isCurrentTrackOver()) {
        playerService.then(function (player) {
          console.log(player, track)
          player.play({
            source: track.key,
            initialPosition: getTrackPosition(track.started)
          });
          player.volume(1);
        });
      } else {
        endTrack();
      }
    }, true);


  });
