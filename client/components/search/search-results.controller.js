'use strict';

angular.module('svperApp')
  .controller('SearchResultsCtrl', function ($scope, rdioService, playerService, $http, socketPromise, $timeout, $q, $rootScope,
    $stateParams) {

    $scope.results = {
      raw: [],
      grouped: []
    };
    $scope.loading = false;
    $scope.numberResults = 0;
    $scope.trackCount = 0;



    $scope.query = function () {
      if (!!$stateParams.q) {
        $scope.loading = true;
        rdioService.searchTracks({
          query: $stateParams.q,
          count: 100
        }).then(function (res) {
          var results = res.data.result.results;

          $scope.results = {
            raw: results,
            grouped: groupArtistNeighbors(results)
          };

          $scope.numberResults = res.data.result.number_results;
          $scope.trackCount = res.data.result.track_count;

          $scope.loading = false;

        });
      }

    };

    $scope.query();

    $scope.more = function () {
      $scope.loading = true;
      rdioService.searchTracks({
        query: $stateParams.q,
        start: $scope.results.raw.length,
        count: 100
      }).then(function (res) {
        var results = res.data.result.results;

        $scope.results.raw = $scope.results.raw.concat(results);

        _.merge($scope.results.grouped, groupArtistNeighbors($scope.results.raw));

        $scope.numberResults = res.data.result.number_results;
        $scope.trackCount = res.data.result.track_count;

        console.log('results', res.data.result)

        $scope.loading = false;

      });
    };

    // $rootScope.$on('search-results:query', function (e, query) {
    //   $scope.queryString = query;
    //   // $scope.query(query)
    // });

    $scope.addToQueue = function (track) {
      console.log(track)
      $rootScope.$broadcast('queue:add', track)
    };

    var groupArtistNeighbors = function (results) {
      var grouped = [];

      _.reduce(results, function(result, n) {

        if(result.artist == n.artist) {
          if (!result.tracks) {
            var newArtistGroup = {
              artist: n.artist,
              tracks: [result, n]
            };
            grouped.push(newArtistGroup);
            return newArtistGroup;
          } else {
            result.tracks.push(n)
            return result;
          }
        } else {
          var newArtistGroup = {
            artist: n.artist,
            tracks: [n]
          };
          grouped.push(newArtistGroup);
          return newArtistGroup;
        }
      });

      return grouped

    };

  });
