'use strict';

angular.module('svperApp')
  .controller('SearchInputCtrl', function ($scope, rdioService, playerService, $http, socketPromise, $timeout, $q, $rootScope,
    $stateParams, $location, $state) {

    $scope.q = $stateParams.q;
    $scope.suggest = function () {
      // rdioService.searchTrackSuggestions({
      //   query: $scope.query
      // }).then(function (res) {
      //   $scope.suggestions = res.data.result;
      // })
    };

    $scope.search = function (q) {
      // $rootScope.$broadcast('search-results:query', query);
      // $scope.suggestions = [];
      $state.go('room.search', {'q': q});

    };

    $scope.addToQueue = function (track) {
      console.log(track)
      $rootScope.$broadcast('queue:add', track)
    };

  });
